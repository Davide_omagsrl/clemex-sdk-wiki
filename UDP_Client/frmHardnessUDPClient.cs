﻿using LogClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UDP_Class;
using ucLed;
using ServerUDP_Config;
using System.Net.NetworkInformation;

namespace Hardness_UDP_Client
{
    public partial class Hardness_Udp_Client : Form
    {
        #region PROPERTIES

        #region Client UDP

        UDP_Class.Client UDP_Client = null;

        #endregion Client UDP

        #region Server Command

        List<CommandsDefinition> commandDefinitionList;

        #endregion Server Command

        #region Alive Message Led

        ucLed.ucLed UcLed1;
        ucLed.ucLed UcLed2;
        bool LedChangeColor;
        Brush brushLedFirst = Brushes.Gray;
        Brush brushLedSecond = Brushes.Green;

        #endregion Alive Message Led

        #region Selected Local Net Address

        public string Local_Net_Address;

        #endregion Selected Local Net Address

        public bool LoadForm = false;

        public int TCPIPServerChangedAddress = 1;

        #endregion PROPERTIES

        #region CONSTRUCTOR

        public Hardness_Udp_Client()
        {
            InitializeComponent();
        }

        #region Dispose

        private void myDispose()
        {
            #region Focus Events dispose

            //foreach (PluginImplementer PI in PIList)
            //{
            //    try
            //    {
            //        PI.ReturnDataPlugin -= PI_EventHandler;
            //    }
            //    catch (Exception)
            //    {
            //    }
            //}

            #endregion Focus Events dispose

            // Dispose of unmanaged resources.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            myDispose();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion Dispose

        #endregion CONSTRUCTOR

        #region METHODS

        #region Init

        public void Init()
        {

            #region Command List and load
            //
            commandDefinitionList = new List<CommandsDefinition>();
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Client_Alive.ToString() + " Parameter:No parameter - Return: No return value", ClientCommand.Client_Alive.ToString(), "", ""));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Get_Scale_Id_List.ToString() + " Parameter:No parameter - Return:List[Scale_Id(Integer)]", ClientCommand.Get_Scale_Id_List.ToString(), "", ""));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Get_Offset.ToString() + " Parameter:Scale_Id(Integer) - Return:List[Description(String), Offset{g}(Double)]", ClientCommand.Get_Offset.ToString(), "30001", ""));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Set_Offset.ToString() + " Parameter:List[step_id(String), offset{g}(Double)] - Return:List[step_id(String), {true/false}(String)]", ClientCommand.Set_Offset.ToString(), "0.098N/0.01kgf|1#1.961N/0.2kgf|1#0.098N/0.01kgf|1#49.03N/5kgf|0", ""));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Get_Light_Level.ToString() + " Parameter:light_id(Integer) - Return:Value{%}(Integer)", ClientCommand.Get_Light_Level.ToString(), "0", ""));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Set_Light_Level.ToString() + " Parameter:light_id(Integer), Light Value{%}(Integer) - Return:{Completed/Error}(String)", ClientCommand.Set_Light_Level.ToString(), "0", ""));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Get_Indexer_Position_List.ToString() + " Parameter:Indexer_Id(Integer) - Return:List[Indexer(Integer), Position(Integer)]", ClientCommand.Get_Indexer_Position_List.ToString(), "0", ""));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Get_Position.ToString() + " Parameter:indexer_id(Integer) - Return:Position(Integer)", ClientCommand.Get_Position.ToString(), "0", ""));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.To_Position.ToString() + " Parameter:Indexer(Integer), Position_id(Integer) Return:{Running} at start rotation {Completed/Error} at end rotation(String)", ClientCommand.To_Position.ToString(), "0", "6"));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Make_Test.ToString() + " Parameter:Scale_Id(Integer), DwellTime{sec}(Integer) Return:{Running} at start test and {Completed/End With Errors/End With Abort} at end Test(String)", ClientCommand.Make_Test.ToString(), "30001", "5"));
            commandDefinitionList.Add(
                new CommandsDefinition(ClientCommand.Emcy_Reset.ToString() + " Parameter:No parameter - Return: No return value", ClientCommand.Emcy_Reset.ToString(), "", ""));
            //
            LoadComboBox();

            #endregion Command List and load

            #region Start and Stop client button

            btnStartClient.Enabled = true;
            btnStopClient.Enabled = false;

            #endregion Start and Stop client button

            #region Alive message led

            UcLed1 = new ucLed.ucLed();
            UcLed2 = new ucLed.ucLed();
            pnlLed1.Controls.Add(UcLed1);
            pnlLed2.Controls.Add(UcLed2);

            #endregion Alive message led

            txtParameter1.Text = "";  
            txtParameter2.Text = "";  
            //
            net_adapters();
            //
            LoadForm = true;
        }

        #endregion Init

        #region Get Network Interface List

        public void net_adapters()    // System.Collections.Generic.List<String> net_adapters()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if ((nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet) || (nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)) //&& (nic.OperationalStatus == OperationalStatus.Up))
                {
                    cboNetworkInterface.Items.Add(nic.Description);
                }
            }
            if (cboNetworkInterface.Items.Count >0) cboNetworkInterface.SelectedIndex = 0;
        }

        #endregion Get Network Interface List

        #region Server Sensor Data

        public void setServerSensorData(string sensorData)
        {
            try
            {
                string[] s = sensorData.Split(':');
                if (s.Length > 1)
                {
                    lblDescrSensorData.Text = s[1];
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion Server Sensor Data

        #region Get TCP IP Address Commented

        //public static string GetLocalIPAddress()
        //{
        //    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
        //    {
        //        var host = Dns.GetHostEntry(Dns.GetHostName());
        //        foreach (var ip in host.AddressList)
        //        {
        //            if (ip.AddressFamily == AddressFamily.InterNetwork)
        //            {
        //                return ip.ToString();
        //            }
        //        }
        //        throw new Exception("No network adapters with an IPv4 address in the system!");
        //    }
        //    return "";
        //}

        #endregion Get TCP IP Address

        #region Load Combo box

        public void LoadComboBox()
        {
            cboCommands.DataSource = commandDefinitionList;
            cboCommands.DisplayMember = "CommandDescription";
        }

        #endregion Load Combo box

        #region end program

        public void endProgram()
        {
            stopClient();
            this.Close();
        }

        #endregion end program

        #region Send To Server

        public void sendToServer(string message)
        {
            UDP_Client.sendString(message);
            DataLog dl = new DataLog();
            dl.MessageType = DataLog.LogType.udp;
            dl.dateTime = DateTime.Now;
            dl.Namespace = "Hardness_UDP_Client";
            dl.ClassNames = "Udp_Client";
            dl.FunctionName = "sendToServer";
            dl.LangMessageFunctionIndex = "sendToServer01";
            dl.LangMessage = "Send to Server:" + message;
            //dl.LangParameterToReplaceInLangMessage
            writeLog(dl);
        }

        #endregion Send To Server

        #region Write Log

        public void writeLog(DataLog dataLog)
        {
            Color foreColor = Color.DarkRed;
            //string pre = dataLog.dateTime.ToString("HH:mm:ss:fff") + " //" + dataLog.Namespace + "/" + dataLog.ClassNames +
            //    "/" + dataLog.FunctionName + " [" + dataLog.MessageType.ToString() + "]";
            string pre = dataLog.dateTime.ToString("HH:mm:ss:fff") + " [" + dataLog.MessageType.ToString() + "]";
            string message = dataLog.LangMessage; // se da convertire utilizzare dataLog.LangMessageFunctionIndex
            int i = 1;
            foreach (string str in dataLog.LangParameterToReplaceInLangMessage)
            {
                message = message.Replace("@" + i++.ToString(), str);
            }
            //
            if (string.IsNullOrEmpty(dataLog.Namespace) && string.IsNullOrEmpty(dataLog.ClassNames) &&
                string.IsNullOrEmpty(dataLog.FunctionName))
            {
                writeLog(message);
            }
            else
            {
                writeLog(pre + " " + message);
            }
        }

        private void writeLog(string message)
        {
            if (txtClientLog.Lines.Count() >= 1000 && txtClientLog.Lines.Count() > 0 && 1000 > 0)  //Max  lines allowed
            {
                //Array.Copy(txtServerLog.Lines, 1, txtServerLog.Lines, 0, txtServerLog.Lines.Length - 1);
                txtClientLog.Select(1, txtClientLog.GetFirstCharIndexFromLine(1)); // Give your line number
                txtClientLog.SelectedText = "";
                //List<string> lines = txtServerLog.Lines.ToList();
                //lines.RemoveAt(0);
                //txtServerLog.Lines = lines.ToArray();
            }
            //
            txtClientLog.Text += message + Environment.NewLine;
            txtClientLog.Select(txtClientLog.Text.Length, 0);
            txtClientLog.Focus();
            txtClientLog.SelectionStart = txtClientLog.Text.Length;
            txtClientLog.ScrollToCaret();
        }

        #endregion Write Log

        #region Change Leds Colors

        public void changeLedColors()
        {
            if (LedChangeColor)
            {
                UcLed1.changeLedColor(brushLedFirst);
                UcLed2.changeLedColor(brushLedSecond);
            }
            else
            {
                UcLed1.changeLedColor(brushLedSecond);
                UcLed2.changeLedColor(brushLedFirst);
            }
            LedChangeColor = !LedChangeColor;
        }

        #endregion Change Leds Colors

        #region Init Client

        public void InitClient()
        {
            #region Client creation

            IPAddress ServerEndPointAddress = IPAddress.Parse(tcpIpAddress.Text);
            //IPAddress ipaLocal = IPAddress.Parse(Local_Net_Address);
            //IPEndPoint ep1 = new IPEndPoint(IPAddress.Any, 12345);
            int p;
            int.TryParse(txtPort.Text, out p);
            if (p > 0 && ! string.IsNullOrEmpty(tcpIpAddress.Text) && ServerEndPointAddress != null)
            {
                if(UDP_Client != null)
                {
                    UDP_Client.ReturnUDPClientLogEvent -= new EventHandler<UDPLogEventArgs>(ucUDP_Log_Event_GoHome);
                    UDP_Client.ReturnUDPClientMessage -= new EventHandler<UDPMessageEventArgs>(ucUDP_Message_GoHome);
                    UDP_Client.stopClient();
                    UDP_Client = null;
                }

                UDP_Client = new Client(Local_Net_Address, ServerEndPointAddress.ToString(), p, Constants.SENSOR_DATA);
                UDP_Client.ReturnUDPClientLogEvent += new EventHandler<UDPLogEventArgs>(ucUDP_Log_Event_GoHome);
                UDP_Client.ReturnUDPClientMessage += new EventHandler<UDPMessageEventArgs>(ucUDP_Message_GoHome);
            }
            else
            {
                DataLog dl = new DataLog();
                dl.dateTime = DateTime.Now;
                dl.MessageType = DataLog.LogType.Error;
                dl.Namespace = "Hardness_UDP_Client";
                dl.ClassNames = "Hardness_Udp_Client";
                dl.FunctionName = "Init";
                dl.LangMessageFunctionIndex = "Init01";
                dl.LangMessage = "Data Error. Check the port or TCP/IP address.";
                //dl.LangParameterToReplaceInLangMessage
                writeLog(dl);

            }

            #endregion Client creation
        }

        #endregion Init Client

        #region Start Client

        public void startClient()
        {
            InitClient();
            //
            DataLog dl = new DataLog();
            dl.dateTime = DateTime.Now;
            dl.Namespace = "Hardness_UDP_Client";
            dl.ClassNames = "Hardness_Udp_Client";
            dl.FunctionName = "btnStartClient_Click";
            //
            if (UDP_Client != null)
            {
                cboNetworkInterface.Enabled = false;
                tcpIpAddress.Enabled = false;
                txtPort.Enabled = false;
                UDP_Client.startClient();
                btnStartClient.Enabled = false;
                btnStopClient.Enabled = true;
                //
                dl.dateTime = DateTime.Now;
                dl.MessageType = DataLog.LogType.udp;
                dl.LangMessageFunctionIndex = "btnStartClient_Click01";
                dl.LangMessage = "Start Client.";
                //dl.LangParameterToReplaceInLangMessage
                writeLog(dl);
            }
            else
            {
                dl.MessageType = DataLog.LogType.Warning;
                dl.LangMessageFunctionIndex = "btnStartClient_Click02";
                dl.LangMessage = "Client not implemented yet.";
                //dl.LangParameterToReplaceInLangMessage
                writeLog(dl);
            }
        }

        #endregion Start Client

        #region Stop Client

        public void stopClient()
        {
            DataLog dl = new DataLog();
            dl.dateTime = DateTime.Now;
            dl.Namespace = "Hardness_UDP_Client";
            dl.ClassNames = "Hardness_Udp_Client";
            dl.FunctionName = "btnStartClient_Click";
            //
            if (UDP_Client != null)
            {
                cboNetworkInterface.Enabled = true;
                tcpIpAddress.Enabled = true;
                txtPort.Enabled = true;
                UDP_Client.stopClient();
                btnStartClient.Enabled = true;
                btnStopClient.Enabled = false;
                //
                dl.dateTime = DateTime.Now;
                dl.MessageType = DataLog.LogType.udp;
                dl.LangMessageFunctionIndex = "btnStartClient_Click01";
                dl.LangMessage = "Stop Client.";
                //dl.LangParameterToReplaceInLangMessage
                writeLog(dl);
                //
                UcLed1.resetLedColor();
                UcLed2.resetLedColor();
                //
            }
            else
            {
                dl.MessageType = DataLog.LogType.Warning;
                dl.LangMessageFunctionIndex = "btnStartClient_Click02";
                dl.LangMessage = "Client not implemented yet.";
                //dl.LangParameterToReplaceInLangMessage
                writeLog(dl);
            }
        }

        #endregion Stop Client

        #endregion METHODS

        #region EVENTS

        #region Load Form

        private void Hardness_Udp_Client_Load(object sender, EventArgs e)
        {
            Init();
        }

        #endregion Load Form

        #region Event raised by UDP_Client Not Used

        public void ucUDP_Message_GoHome(object sender, UDPMessageEventArgs e)
        {
            DataLog dl = new DataLog();
            dl.dateTime = DateTime.Now;
            dl.MessageType = DataLog.LogType.Info;
            dl.Namespace = "Hardness_UDP_Client";
            dl.ClassNames = "Hardness_Udp_Client";
            //
            ReturnUdpDataMessageGoHome rudmgh = e.UdpMessageGoHome;
            //
            dl.FunctionName = "ucUDP_Message_GoHome";
            dl.LangMessageFunctionIndex = "ucUDP_Message_GoHome01";
            dl.LangMessage = rudmgh.command;
            //dl.LangParameterToReplaceInLangMessage
            writeLog(dl);
            //
            dl.LangMessageFunctionIndex = "ucUDP_Message_GoHome02";
            dl.LangMessage = rudmgh.udpReceivedAddress + " [" + rudmgh.ipEndPoint.ToString() + "]";
            //dl.LangParameterToReplaceInLangMessage
            writeLog(dl);
            //
            dl.LangMessageFunctionIndex = "ucUDP_Message_GoHome02";
            dl.LangMessage = rudmgh.udpMessage;
            //dl.LangParameterToReplaceInLangMessage
            writeLog(dl);
            //
            switch (rudmgh.command)
            {
                //case "Alive":
                //    changeLedColors();
                //    break;
                case "Udp Message":
                    switch (rudmgh.udpMessage)
                    {
                        case "Hardness Server UDP Alive":
                            changeLedColors();
                            break;
                        case "Client message":
                            break;
                        case "Message":
                            break;
                        case "Save and go home":
                            break;  // Not Used
                        case "Go Home":
                            break;  // Not Used
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        #endregion Event raised by UDP_Client

        #region Event Log raised by UDP_Client

        public void ucUDP_Log_Event_GoHome(object sender, UDPLogEventArgs e)
        {
            ReturnLogDataGoHome rldgh = e.LogGoHome;
            //
            if(rldgh.DataLog.LangMessage.Contains(Constants.ALIVE_MESSAGE))
            {
                changeLedColors();
            }
            else if (rldgh.DataLog.LangMessage.Contains(Constants.SENSOR_DATA))
            {
                setServerSensorData(rldgh.DataLog.LangMessage);
            }
            else
            {
                switch (rldgh.DataLog.LangMessage)
                {
                    default:
                        writeLog(rldgh.DataLog);
                        break;
                }
            }
            //
        }

        #endregion Event Log raised by UDP_Client

        #region Buttons

        #region Get Server Command To Send

        private void btnGetCommandToSend_Click(object sender, EventArgs e)
        {
            if(cboCommands.SelectedIndex >= 0)
            {
                try
                {
                    CommandsDefinition cd = (CommandsDefinition)cboCommands.SelectedItem;
                    if (cd != null && ! string.IsNullOrEmpty(cd.CommandValue))
                    {
                        txtSend.Text = cd.CommandValue;
                        if (!string.IsNullOrEmpty(txtParameter1.Text))
                        {
                            txtSend.Text += ":" + txtParameter1.Text;
                            //
                            if (!string.IsNullOrEmpty(txtParameter2.Text))
                            {
                                txtSend.Text += ";" + txtParameter2.Text;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DataLog dl = new DataLog();
                    dl.dateTime = DateTime.Now;
                    dl.MessageType = DataLog.LogType.Warning;
                    dl.Namespace = "Hardness_UDP_Client";
                    dl.ClassNames = "Hardness_Udp_Client";
                    dl.FunctionName = "btnGetCommandToSend_Click";
                    dl.LangMessageFunctionIndex = "btnGetCommandToSend_Click01";
                    dl.LangMessage = "Error:@1.";
                    dl.LangParameterToReplaceInLangMessage.Add(ex.Message);
                    writeLog(dl);
                }
            }
        }

        #endregion Get Server Command To Send

        #region Send Command To Server

        private void btnSend_Click(object sender, EventArgs e)
        {
            DataLog dl = new DataLog();
            dl.dateTime = DateTime.Now;
            dl.MessageType = DataLog.LogType.Warning;
            dl.Namespace = "Hardness_UDP_Client";
            dl.ClassNames = "Hardness_Udp_Client";
            dl.FunctionName = "btnSend_Click";
            if (! btnStartClient.Enabled)
            {
                if(! string.IsNullOrEmpty(txtSend.Text))
                {
                    sendToServer(txtSend.Text);
                }
                else
                {
                    dl.LangMessageFunctionIndex = "btnSend_Click01";
                    dl.LangMessage = "Data to send empty. Operation Aborted.";
                    //dl.LangParameterToReplaceInLangMessage
                    writeLog(dl);
                }
            }
            else
            {
                dl.LangMessageFunctionIndex = "btnSend_Click02";
                dl.LangMessage = "Client not running.";
                //dl.LangParameterToReplaceInLangMessage
                writeLog(dl);
            }
        }

        #endregion Send Command To Server

        #region Start Client

        private void btnStartClient_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(tcpIpAddress.Text))
            {
                IPAddress ipa;
                if(IPAddress.TryParse(tcpIpAddress.Text, out ipa))
                {
                    startClient();
                }
            }
        }

        #endregion Start Client

        #region Stop Client

        private void btnStopClient_Click(object sender, EventArgs e)
        {
            stopClient();
        }

        #endregion Stop Client

        #region Clear Log

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            txtClientLog.Text ="";
        }

        #endregion Clear Log

        #region Close program

        private void btnClose_Click(object sender, EventArgs e)
        {
            endProgram();
        }

        #endregion Close program

        #region TCP/IP Address

        private void btnTCPIPAddress_Click(object sender, EventArgs e)
        {
            switch (TCPIPServerChangedAddress)
            {
                default:
                case 0:
                    break;
                case 1:
                    tcpIpAddress.Text = "192.168.1.109";
                    break;
                case 2:
                    tcpIpAddress.Text = "169.254.236.209"; 
                    break;
                case 3:
                    tcpIpAddress.Text = "192.168.1.198";
                    break;
            }
            txtPort.Text = "10030";
            if (TCPIPServerChangedAddress == 3) TCPIPServerChangedAddress = 0;
            TCPIPServerChangedAddress++;
        }

        #endregion TCP/IP Address

        #endregion Buttons

        #region Combo box

        #region Command

        private void cboCommands_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommandsDefinition cd = (CommandsDefinition)cboCommands.SelectedItem;
            txtParameter1.Text =cd.Parameter1;
            txtParameter2.Text = cd.Parameter2;
            if(cd.CommandValue == ClientCommand.Set_Light_Level.ToString())
            {
                Random random = new Random();
                cd.Parameter2 = random.Next(0, 100).ToString();
            }
            lblDescrCommand.Focus();
        }

        private void cboCommands_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        #endregion Command

        #region Network Interface

        private void cboNetworkInterface_SelectedIndexChanged(object sender, EventArgs e)
        {

            #region Get TCP/IP address and set to form header

            try
            {
                if(LoadForm)
                {
                    //NetworkInterface[] ni = NetworkInterface.GetAllNetworkInterfaces();
                    //List<NetworkInterface> niList = ni.ToList();
                    //NetworkInterface n = niList.Where(o => o.Name == cboNetworkInterface.SelectedItem.ToString()).FirstOrDefault();
                    //IPInterfaceProperties ipip = n.GetIPProperties();  //.UnicastAddresses;
                    //IPv4InterfaceProperties ipv4ip = ipip.GetIPv4Properties();
                    //Local_Net_Address = ipv4ip.ToString();   //GetLocalIPAddress();
                    //this.Text = "UDP Client - TCP/IP:" + Local_Net_Address;

                    foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                    {
                        foreach (UnicastIPAddressInformation ip in nic.GetIPProperties().UnicastAddresses)
                        {
                            if (nic.Description == cboNetworkInterface.SelectedItem.ToString())
                            {
                                if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                                {
                                    //MessageBox.Show(ip.Address.ToString());
                                    Local_Net_Address = ip.Address.ToString();   //GetLocalIPAddress();
                                    this.Text = "UDP Client - TCP/IP:" + Local_Net_Address;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            #endregion Get TCP/IP address and set to form header
        }

        #endregion Command

        #endregion Combo box

        #region Text Box

        private void txtClientLog_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        #endregion Text Box

        #endregion EVENTS
    }

    #region Command Class

    public class CommandsDefinition
    {

        #region PROPERTIES

        public string CommandDescription {get; set;}
        public string CommandValue { get; set; }

        public string Parameter1 { get; set; }

        public string Parameter2 { get; set; }

        #endregion PROPERTIES

        #region CONSTRUCTOR

        public CommandsDefinition()
        {

        }

        public CommandsDefinition(string commandDescription, string commandValue, string parameter1, string parameter2)
        {
            CommandDescription = commandDescription;
            CommandValue = commandValue;
            Parameter1 = parameter1;
            Parameter2 = parameter2;
        }

        #endregion CONSTRUCTOR

    }

    #endregion Command Class

}
