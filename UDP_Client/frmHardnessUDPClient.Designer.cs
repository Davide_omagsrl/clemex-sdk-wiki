﻿namespace Hardness_UDP_Client
{
    partial class Hardness_Udp_Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        ///// <summary>
        ///// Clean up any resources being used.
        ///// </summary>
        ///// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.txtClientLog = new System.Windows.Forms.TextBox();
            this.txtSend = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.lblDescrTCPIPAddress = new System.Windows.Forms.Label();
            this.btnStopClient = new System.Windows.Forms.Button();
            this.btnStartClient = new System.Windows.Forms.Button();
            this.lblDescrPort = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.tcpIpAddress = new iptb.TCPIPAddressTextBox();
            this.pnlStart_Stop = new System.Windows.Forms.Panel();
            this.lblDescrNetworkInterface = new System.Windows.Forms.Label();
            this.cboNetworkInterface = new System.Windows.Forms.ComboBox();
            this.gbSensorData = new System.Windows.Forms.GroupBox();
            this.lblDescrSensorData = new System.Windows.Forms.Label();
            this.gbTimespanAliveMessage = new System.Windows.Forms.GroupBox();
            this.pnlLed2 = new System.Windows.Forms.Panel();
            this.pnlLed1 = new System.Windows.Forms.Panel();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.pnlSend = new System.Windows.Forms.Panel();
            this.gbSendToServer = new System.Windows.Forms.GroupBox();
            this.gbCommandSelection = new System.Windows.Forms.GroupBox();
            this.lblDescrParameter2 = new System.Windows.Forms.Label();
            this.lblDescrParameter1 = new System.Windows.Forms.Label();
            this.lblDescrCommand = new System.Windows.Forms.Label();
            this.txtParameter2 = new System.Windows.Forms.TextBox();
            this.btnGetCommandToSend = new System.Windows.Forms.Button();
            this.txtParameter1 = new System.Windows.Forms.TextBox();
            this.cboCommands = new System.Windows.Forms.ComboBox();
            this.pnlLog = new System.Windows.Forms.Panel();
            this.btnTCPIPAddress = new System.Windows.Forms.Button();
            this.pnlStart_Stop.SuspendLayout();
            this.gbSensorData.SuspendLayout();
            this.gbTimespanAliveMessage.SuspendLayout();
            this.pnlSend.SuspendLayout();
            this.gbSendToServer.SuspendLayout();
            this.gbCommandSelection.SuspendLayout();
            this.pnlLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(995, 10);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(47, 38);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtClientLog
            // 
            this.txtClientLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtClientLog.Location = new System.Drawing.Point(7, 3);
            this.txtClientLog.Multiline = true;
            this.txtClientLog.Name = "txtClientLog";
            this.txtClientLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtClientLog.Size = new System.Drawing.Size(1035, 394);
            this.txtClientLog.TabIndex = 2;
            this.txtClientLog.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClientLog_KeyPress);
            // 
            // txtSend
            // 
            this.txtSend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSend.Location = new System.Drawing.Point(9, 14);
            this.txtSend.Multiline = true;
            this.txtSend.Name = "txtSend";
            this.txtSend.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSend.Size = new System.Drawing.Size(874, 43);
            this.txtSend.TabIndex = 4;
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.Location = new System.Drawing.Point(889, 14);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(141, 43);
            this.btnSend.TabIndex = 5;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // lblDescrTCPIPAddress
            // 
            this.lblDescrTCPIPAddress.AutoSize = true;
            this.lblDescrTCPIPAddress.Location = new System.Drawing.Point(715, 9);
            this.lblDescrTCPIPAddress.Name = "lblDescrTCPIPAddress";
            this.lblDescrTCPIPAddress.Size = new System.Drawing.Size(121, 13);
            this.lblDescrTCPIPAddress.TabIndex = 9;
            this.lblDescrTCPIPAddress.Text = "Server TCP/IP Address.";
            // 
            // btnStopClient
            // 
            this.btnStopClient.Location = new System.Drawing.Point(344, 9);
            this.btnStopClient.Name = "btnStopClient";
            this.btnStopClient.Size = new System.Drawing.Size(55, 38);
            this.btnStopClient.TabIndex = 7;
            this.btnStopClient.Text = "Stop Client";
            this.btnStopClient.UseVisualStyleBackColor = true;
            this.btnStopClient.Click += new System.EventHandler(this.btnStopClient_Click);
            // 
            // btnStartClient
            // 
            this.btnStartClient.Location = new System.Drawing.Point(283, 9);
            this.btnStartClient.Name = "btnStartClient";
            this.btnStartClient.Size = new System.Drawing.Size(55, 38);
            this.btnStartClient.TabIndex = 6;
            this.btnStartClient.Text = "Start Client";
            this.btnStartClient.UseVisualStyleBackColor = true;
            this.btnStartClient.Click += new System.EventHandler(this.btnStartClient_Click);
            // 
            // lblDescrPort
            // 
            this.lblDescrPort.AutoSize = true;
            this.lblDescrPort.Location = new System.Drawing.Point(867, 9);
            this.lblDescrPort.Name = "lblDescrPort";
            this.lblDescrPort.Size = new System.Drawing.Size(75, 13);
            this.lblDescrPort.TabIndex = 11;
            this.lblDescrPort.Text = "Server Port nr.";
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(873, 25);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(61, 20);
            this.txtPort.TabIndex = 10;
            // 
            // tcpIpAddress
            // 
            this.tcpIpAddress.Location = new System.Drawing.Point(711, 25);
            this.tcpIpAddress.Name = "tcpIpAddress";
            this.tcpIpAddress.Size = new System.Drawing.Size(132, 18);
            this.tcpIpAddress.TabIndex = 12;
            this.tcpIpAddress.ToolTipText = "";
            // 
            // pnlStart_Stop
            // 
            this.pnlStart_Stop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlStart_Stop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStart_Stop.Controls.Add(this.btnTCPIPAddress);
            this.pnlStart_Stop.Controls.Add(this.lblDescrNetworkInterface);
            this.pnlStart_Stop.Controls.Add(this.cboNetworkInterface);
            this.pnlStart_Stop.Controls.Add(this.gbSensorData);
            this.pnlStart_Stop.Controls.Add(this.gbTimespanAliveMessage);
            this.pnlStart_Stop.Controls.Add(this.btnClearLog);
            this.pnlStart_Stop.Controls.Add(this.btnClose);
            this.pnlStart_Stop.Controls.Add(this.btnStopClient);
            this.pnlStart_Stop.Controls.Add(this.tcpIpAddress);
            this.pnlStart_Stop.Controls.Add(this.btnStartClient);
            this.pnlStart_Stop.Controls.Add(this.lblDescrPort);
            this.pnlStart_Stop.Controls.Add(this.lblDescrTCPIPAddress);
            this.pnlStart_Stop.Controls.Add(this.txtPort);
            this.pnlStart_Stop.Location = new System.Drawing.Point(5, 541);
            this.pnlStart_Stop.Name = "pnlStart_Stop";
            this.pnlStart_Stop.Size = new System.Drawing.Size(1051, 59);
            this.pnlStart_Stop.TabIndex = 13;
            // 
            // lblDescrNetworkInterface
            // 
            this.lblDescrNetworkInterface.AutoSize = true;
            this.lblDescrNetworkInterface.Location = new System.Drawing.Point(497, 7);
            this.lblDescrNetworkInterface.Name = "lblDescrNetworkInterface";
            this.lblDescrNetworkInterface.Size = new System.Drawing.Size(92, 13);
            this.lblDescrNetworkInterface.TabIndex = 17;
            this.lblDescrNetworkInterface.Text = "Network Interface";
            // 
            // cboNetworkInterface
            // 
            this.cboNetworkInterface.FormattingEnabled = true;
            this.cboNetworkInterface.Location = new System.Drawing.Point(405, 23);
            this.cboNetworkInterface.Name = "cboNetworkInterface";
            this.cboNetworkInterface.Size = new System.Drawing.Size(300, 21);
            this.cboNetworkInterface.TabIndex = 16;
            this.cboNetworkInterface.SelectedIndexChanged += new System.EventHandler(this.cboNetworkInterface_SelectedIndexChanged);
            // 
            // gbSensorData
            // 
            this.gbSensorData.Controls.Add(this.lblDescrSensorData);
            this.gbSensorData.Location = new System.Drawing.Point(143, 4);
            this.gbSensorData.Name = "gbSensorData";
            this.gbSensorData.Size = new System.Drawing.Size(130, 48);
            this.gbSensorData.TabIndex = 15;
            this.gbSensorData.TabStop = false;
            this.gbSensorData.Text = "Sensor Data";
            // 
            // lblDescrSensorData
            // 
            this.lblDescrSensorData.AutoSize = true;
            this.lblDescrSensorData.Location = new System.Drawing.Point(6, 22);
            this.lblDescrSensorData.Name = "lblDescrSensorData";
            this.lblDescrSensorData.Size = new System.Drawing.Size(101, 13);
            this.lblDescrSensorData.TabIndex = 0;
            this.lblDescrSensorData.Text = "lblDescrSensorData";
            // 
            // gbTimespanAliveMessage
            // 
            this.gbTimespanAliveMessage.Controls.Add(this.pnlLed2);
            this.gbTimespanAliveMessage.Controls.Add(this.pnlLed1);
            this.gbTimespanAliveMessage.Location = new System.Drawing.Point(7, 3);
            this.gbTimespanAliveMessage.Name = "gbTimespanAliveMessage";
            this.gbTimespanAliveMessage.Size = new System.Drawing.Size(130, 48);
            this.gbTimespanAliveMessage.TabIndex = 14;
            this.gbTimespanAliveMessage.TabStop = false;
            this.gbTimespanAliveMessage.Text = "Alive Message";
            // 
            // pnlLed2
            // 
            this.pnlLed2.Location = new System.Drawing.Point(72, 14);
            this.pnlLed2.Name = "pnlLed2";
            this.pnlLed2.Size = new System.Drawing.Size(41, 30);
            this.pnlLed2.TabIndex = 1;
            // 
            // pnlLed1
            // 
            this.pnlLed1.Location = new System.Drawing.Point(28, 14);
            this.pnlLed1.Name = "pnlLed1";
            this.pnlLed1.Size = new System.Drawing.Size(41, 30);
            this.pnlLed1.TabIndex = 0;
            // 
            // btnClearLog
            // 
            this.btnClearLog.Location = new System.Drawing.Point(942, 10);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(47, 38);
            this.btnClearLog.TabIndex = 13;
            this.btnClearLog.Text = "Clear Log";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // pnlSend
            // 
            this.pnlSend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSend.Controls.Add(this.gbSendToServer);
            this.pnlSend.Controls.Add(this.gbCommandSelection);
            this.pnlSend.Location = new System.Drawing.Point(5, 3);
            this.pnlSend.Name = "pnlSend";
            this.pnlSend.Size = new System.Drawing.Size(1051, 127);
            this.pnlSend.TabIndex = 14;
            // 
            // gbSendToServer
            // 
            this.gbSendToServer.Controls.Add(this.txtSend);
            this.gbSendToServer.Controls.Add(this.btnSend);
            this.gbSendToServer.Location = new System.Drawing.Point(7, 59);
            this.gbSendToServer.Name = "gbSendToServer";
            this.gbSendToServer.Size = new System.Drawing.Size(1036, 63);
            this.gbSendToServer.TabIndex = 15;
            this.gbSendToServer.TabStop = false;
            this.gbSendToServer.Text = "Send to Server";
            // 
            // gbCommandSelection
            // 
            this.gbCommandSelection.Controls.Add(this.lblDescrParameter2);
            this.gbCommandSelection.Controls.Add(this.lblDescrParameter1);
            this.gbCommandSelection.Controls.Add(this.lblDescrCommand);
            this.gbCommandSelection.Controls.Add(this.txtParameter2);
            this.gbCommandSelection.Controls.Add(this.btnGetCommandToSend);
            this.gbCommandSelection.Controls.Add(this.txtParameter1);
            this.gbCommandSelection.Controls.Add(this.cboCommands);
            this.gbCommandSelection.Location = new System.Drawing.Point(7, 7);
            this.gbCommandSelection.Name = "gbCommandSelection";
            this.gbCommandSelection.Size = new System.Drawing.Size(1036, 50);
            this.gbCommandSelection.TabIndex = 15;
            this.gbCommandSelection.TabStop = false;
            this.gbCommandSelection.Text = "Command Selection";
            // 
            // lblDescrParameter2
            // 
            this.lblDescrParameter2.AutoSize = true;
            this.lblDescrParameter2.Location = new System.Drawing.Point(763, 22);
            this.lblDescrParameter2.Name = "lblDescrParameter2";
            this.lblDescrParameter2.Size = new System.Drawing.Size(67, 13);
            this.lblDescrParameter2.TabIndex = 18;
            this.lblDescrParameter2.Text = "Parameter 2:";
            // 
            // lblDescrParameter1
            // 
            this.lblDescrParameter1.AutoSize = true;
            this.lblDescrParameter1.Location = new System.Drawing.Point(576, 22);
            this.lblDescrParameter1.Name = "lblDescrParameter1";
            this.lblDescrParameter1.Size = new System.Drawing.Size(67, 13);
            this.lblDescrParameter1.TabIndex = 17;
            this.lblDescrParameter1.Text = "Parameter 1:";
            // 
            // lblDescrCommand
            // 
            this.lblDescrCommand.AutoSize = true;
            this.lblDescrCommand.Location = new System.Drawing.Point(6, 22);
            this.lblDescrCommand.Name = "lblDescrCommand";
            this.lblDescrCommand.Size = new System.Drawing.Size(57, 13);
            this.lblDescrCommand.TabIndex = 16;
            this.lblDescrCommand.Text = "Command:";
            // 
            // txtParameter2
            // 
            this.txtParameter2.Location = new System.Drawing.Point(830, 19);
            this.txtParameter2.Name = "txtParameter2";
            this.txtParameter2.Size = new System.Drawing.Size(112, 20);
            this.txtParameter2.TabIndex = 15;
            // 
            // btnGetCommandToSend
            // 
            this.btnGetCommandToSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetCommandToSend.Location = new System.Drawing.Point(949, 9);
            this.btnGetCommandToSend.Name = "btnGetCommandToSend";
            this.btnGetCommandToSend.Size = new System.Drawing.Size(81, 38);
            this.btnGetCommandToSend.TabIndex = 6;
            this.btnGetCommandToSend.Text = "GetCommandToSend";
            this.btnGetCommandToSend.UseVisualStyleBackColor = true;
            this.btnGetCommandToSend.Click += new System.EventHandler(this.btnGetCommandToSend_Click);
            // 
            // txtParameter1
            // 
            this.txtParameter1.Location = new System.Drawing.Point(643, 19);
            this.txtParameter1.Name = "txtParameter1";
            this.txtParameter1.Size = new System.Drawing.Size(112, 20);
            this.txtParameter1.TabIndex = 14;
            // 
            // cboCommands
            // 
            this.cboCommands.FormattingEnabled = true;
            this.cboCommands.Location = new System.Drawing.Point(66, 19);
            this.cboCommands.Name = "cboCommands";
            this.cboCommands.Size = new System.Drawing.Size(504, 21);
            this.cboCommands.TabIndex = 13;
            this.cboCommands.SelectedIndexChanged += new System.EventHandler(this.cboCommands_SelectedIndexChanged);
            this.cboCommands.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCommands_KeyPress);
            // 
            // pnlLog
            // 
            this.pnlLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLog.Controls.Add(this.txtClientLog);
            this.pnlLog.Location = new System.Drawing.Point(5, 135);
            this.pnlLog.Name = "pnlLog";
            this.pnlLog.Size = new System.Drawing.Size(1051, 402);
            this.pnlLog.TabIndex = 16;
            // 
            // btnTCPIPAddress
            // 
            this.btnTCPIPAddress.Location = new System.Drawing.Point(844, 26);
            this.btnTCPIPAddress.Name = "btnTCPIPAddress";
            this.btnTCPIPAddress.Size = new System.Drawing.Size(23, 18);
            this.btnTCPIPAddress.TabIndex = 18;
            this.btnTCPIPAddress.UseVisualStyleBackColor = true;
            this.btnTCPIPAddress.Click += new System.EventHandler(this.btnTCPIPAddress_Click);
            // 
            // Hardness_Udp_Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 601);
            this.Controls.Add(this.pnlLog);
            this.Controls.Add(this.pnlSend);
            this.Controls.Add(this.pnlStart_Stop);
            this.Name = "Hardness_Udp_Client";
            this.Text = "UDP_Client";
            this.Load += new System.EventHandler(this.Hardness_Udp_Client_Load);
            this.pnlStart_Stop.ResumeLayout(false);
            this.pnlStart_Stop.PerformLayout();
            this.gbSensorData.ResumeLayout(false);
            this.gbSensorData.PerformLayout();
            this.gbTimespanAliveMessage.ResumeLayout(false);
            this.pnlSend.ResumeLayout(false);
            this.gbSendToServer.ResumeLayout(false);
            this.gbSendToServer.PerformLayout();
            this.gbCommandSelection.ResumeLayout(false);
            this.gbCommandSelection.PerformLayout();
            this.pnlLog.ResumeLayout(false);
            this.pnlLog.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtClientLog;
        private System.Windows.Forms.TextBox txtSend;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label lblDescrTCPIPAddress;
        private System.Windows.Forms.Button btnStopClient;
        private System.Windows.Forms.Button btnStartClient;
        private System.Windows.Forms.Label lblDescrPort;
        private System.Windows.Forms.TextBox txtPort;
        private iptb.TCPIPAddressTextBox tcpIpAddress;
        private System.Windows.Forms.Panel pnlStart_Stop;
        private System.Windows.Forms.Panel pnlSend;
        private System.Windows.Forms.Panel pnlLog;
        private System.Windows.Forms.TextBox txtParameter1;
        private System.Windows.Forms.Button btnGetCommandToSend;
        private System.Windows.Forms.ComboBox cboCommands;
        private System.Windows.Forms.GroupBox gbSendToServer;
        private System.Windows.Forms.GroupBox gbCommandSelection;
        private System.Windows.Forms.Label lblDescrParameter2;
        private System.Windows.Forms.Label lblDescrParameter1;
        private System.Windows.Forms.Label lblDescrCommand;
        private System.Windows.Forms.TextBox txtParameter2;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.GroupBox gbTimespanAliveMessage;
        private System.Windows.Forms.Panel pnlLed1;
        private System.Windows.Forms.Panel pnlLed2;
        private System.Windows.Forms.GroupBox gbSensorData;
        public System.Windows.Forms.Label lblDescrSensorData;
        private System.Windows.Forms.Label lblDescrNetworkInterface;
        private System.Windows.Forms.ComboBox cboNetworkInterface;
        private System.Windows.Forms.Button btnTCPIPAddress;
    }
}

